const gulp = require('gulp');
const eslint = require('gulp-eslint');
 
gulp.task('lint', () => {
    return gulp.src(['js/**/*.js','!node_modules/**'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('watch', function() {
    gulp.watch(['js/**/*.js'], ['lint']);
});

gulp.task('karma', function(done){
  var config = {
    testDir: 'test'
    };
    var karma = require('karma');  
    var karmaOpts = {
      configFile:  process.cwd() + "/" +   config.testDir + '/karma.conf.js',
      singleRun: false,
    };
    new karma.Server(karmaOpts, done).start();
});

 