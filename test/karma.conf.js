module.exports = function(config) {
  config.set({
    basePath: '../',
    frameworks: ['jasmine', 'requirejs'],
    files: [
      {pattern: 'js/specs/*spec.js', included: false},
      {pattern: 'js/**/*.js', included: false},
      {pattern: 'node_modules/jquery/dist/jquery.js', included: false},
      {pattern:'node_modules/underscore/underscore.js',included: false},
      {pattern:'node_modules/backbone/backbone.js',included: false},
      {pattern:'node_modules/backbone.localstorage/backbone.localStorage.js',included: false},
      {pattern:'node_modules/requirejs-text/text.js',included: false},
      'test/main.test.js'
    ],
    exclude: [
      'js/main.js'
    ],
    preprocessors: {
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    browsers: [
      'Chrome',
    ],
    singleRun: false,
    autoWatch: true,  
    concurrency: Infinity
  });
}
