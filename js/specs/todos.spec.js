define(['collections/todos','jquery', 'underscore'], function(Todos, $, _) {

    describe('just checking', function() {

        it('works for collection', function() {
            Todos.add({title: 'Example'});            
            expect(Todos.length > 0).toBe(true);
        });        

        it('works for underscore', function() {
            // just checking that _ works
            expect(_.size([1,2,3])).toEqual(3);
        });

    });

});